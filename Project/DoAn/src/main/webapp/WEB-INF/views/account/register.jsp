<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form"%>


<head>

<link
	href="${pageContext.request.contextPath }/resources/admin/dist/css/style.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath }/resources/admin/dist/css/fontawesome-all.css"
	rel="stylesheet" />
<link
	href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
	rel="stylesheet">
</head>


<body>

	<h1>Admin Register</h1>

	<s:form action="${pageContext.request.contextPath }/account/register" modelAttribute="account" method="post">
		<div class=" w3l-login-form">
			<h2>Register Here</h2>
			<div class=" w3l-form-group">
				<label>Username:</label>
				<div class="group">
					<i class="fas fa-user"></i> <input type="text" class="form-control"
						name="username" required="required" placeholder="Username"/>

				</div>
			</div>
			<div class=" w3l-form-group">
				<label>Password:</label>
				<div class="group">
					<i class="fas fa-unlock"></i> <input type="password"
						class="form-control" name="password" required="required" placeholder="Password"/>
				</div>
			</div>
			<div class=" w3l-form-group">
				<label>FullName:</label>
				<div class="group">
					<i class="fas fa-user"></i> <input type="text" class="form-control"
						name="fullName" required="required" placeholder="Full Name" />
				</div>
			</div>
			<div class=" w3l-form-group">
				<label>Phone Number:</label>
				<div class="group">
					<i class="fas fa-user"></i> <input type="text" class="form-control"
						name="phoneNumber" required="required" placeholder="Phone Number" maxlength="10"/>
				</div>
			</div>
			<div class=" w3l-form-group">
				<label>Roles:</label>
				<ul>
					<c:forEach var="role" items="${roles }">
						<li><input type="checkbox" name="roles" value="${role.id }">${role.name }
						</li>
					</c:forEach>
				</ul>
			</div>
			<button type="submit" value="register">Register</button>
			<p class=" w3l-register-p">
				<a
					href="${pageContext.request.contextPath }/account/login"
					class="register"> Login</a>
			</p>
		</div>

	</s:form>
</body>

</html>