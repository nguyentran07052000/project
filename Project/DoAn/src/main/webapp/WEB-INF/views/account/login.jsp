<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
    <link href="${pageContext.request.contextPath }/resources/admin/dist/css/style.css" rel="stylesheet" type="text/css" />

    <link href="${pageContext.request.contextPath }/resources/admin/dist/css/fontawesome-all.css" rel="stylesheet" />

</head>


<body>
    <h1>Admin Login Form</h1>
    <div class=" w3l-login-form">
        <h2>Login Here</h2>
        <form  method="post" action="${pageContext.request.contextPath }/account/process-login">

            <div class=" w3l-form-group">
                <label>Username:</label>
                <div class="group">
                    <i class="fas fa-user"></i>
                    <input type="text" class="form-control" name="username" placeholder="username" required="required" />
                </div>
            </div>
            <div class=" w3l-form-group">
                <label>Password:</label>
                <div class="group">
                    <i class="fas fa-unlock"></i>
                    <input type="password" class="form-control" name="password" placeholder="password" required="required" />
                </div>
            </div>
            
            <button type="submit" value="login">Login</button>
            <div class=" w3l-form-group">
                <a style="color: red;">${msg }</a>
            </div>
        </form>
        <p class=" w3l-register-p">Don't have an account?<a href="${pageContext.request.contextPath }/account/register" class="register"> Register</a></p>
    </div>
   
</body>

</html>