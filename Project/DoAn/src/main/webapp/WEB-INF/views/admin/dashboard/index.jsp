<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h3>Dashboard Page</h3>
	Welcome ${username }
	<ul>
		<sec:authorize access="hasRole('ROLE_SUPERADMIN')">
			<li>Menu 1</li>
		</sec:authorize>
		<sec:authorize access="hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_ADMIN')">
			<li>Menu 2</li>
		</sec:authorize>
	</ul>
</body>
</html>