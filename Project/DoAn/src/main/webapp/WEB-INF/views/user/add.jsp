<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>

	<h3>Register</h3>
	<s:form method="post" modelAttribute="user"
		action="${pageContext.request.contextPath }/user/add">
		<table>
			<tr>
				<td>Username</td>
				<td><s:input path="username" /></td>
			</tr>
			<tr>
				<td>Password</td>
				<td>
					<s:password path="password" cols="20" rows="5" />
				</td>
			</tr>
			<tr>
				<td>FullName</td>
				<td><s:input path="fullName" /></td>
			</tr>
			<tr>
				<td>Roles</td>
				<td>
								
				<s:checkbox path="roles" value="8"/>ADMIN
				<br>
				<s:checkbox path="roles" value="9"/>USER
				</td>
			</tr>
			
			<tr>
				<td>&nbsp;</td>
				<td>
					<input type="submit" value="add"/>
				</td>
			</tr>
		</table>
	</s:form>
</body>
</html>