<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h3>Support Online</h3>

	<a href="${pageContext.request.contextPath }/user/add">Add</a>
	<table border="1">
		<tr>
			<th>Id</th>
			<th>Username</th>			
			<th>FullName</th>
			<th>Roles</th>
			<th>Action</th>
		</tr>
		<c:choose>
			<c:when test="${users.size() > 0}">
				<c:forEach items="${users}" var="user">
					
					<c:if test="${user.id != 16}">
							<tr>
						<td >${user.id}</td>
						<td>${user.username}</td>
						<td>${user.fullName}</td>
						<td>
							<c:forEach var="us" items="${user.roles }">
								${us.name }
							</c:forEach>
						</td>
						<td><a onclick="return comfirm('Are you sure?')"
							href="${pageContext.request.contextPath }/user/delete/${user.id}">Delete</a>
							<a href="${pageContext.request.contextPath }/user/edit/${user.id}">Edit</a>

						</td>
						</tr>
						</c:if>
					
					
				</c:forEach>
			</c:when>
			<c:otherwise>
				<tr align="center">
					<td colspan="8">No Users available</td>
				</tr>
			</c:otherwise>
		</c:choose>
	</table>
</body>
</html>