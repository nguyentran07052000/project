package com.demo.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.demo.models.Account;

@Component("accountValidation")
public class AccountValidation  implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		
		return clazz.equals(Account.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		
	}

}
