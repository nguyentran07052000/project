package com.demo.controller.admin;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping({"admin/dashboard"})
public class DashboardController {

	@RequestMapping(method = RequestMethod.GET)
	public String index(Authentication authentication, ModelMap modelMap) {
		modelMap.put("username", authentication.getName());
		return "admin/dashboard/index";
	}
}
