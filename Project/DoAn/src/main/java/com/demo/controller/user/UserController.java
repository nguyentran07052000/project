package com.demo.controller.user;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demo.models.Account;
import com.demo.rervices.AccountServices;


@Controller
@RequestMapping({"user"})
public class UserController {
	@Autowired
	private AccountServices acountServices;
	
	@RequestMapping(value = "index", method = RequestMethod.GET)
	public String index(ModelMap modelMap) {
		modelMap.put("users", acountServices.findAll());
		return  "user/index";
	}
	
	@RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") int id) {
		acountServices.delete(id);
		return  "redirect:/user/index";
	}
	
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String add(ModelMap modelMap) {
		Account account = new Account();
		modelMap.put("user", account);
		return  "user/add";
	}
	
	@RequestMapping(value = "add", method = RequestMethod.POST)
	public String add(@ModelAttribute("user:")  Account account) {
		String hash = new BCryptPasswordEncoder().encode(account.getPassword());
		account.setPassword(hash);
		acountServices.save(account);
		return  "redirect:/user/index";
	}
	
	@RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable("id") int id, ModelMap modelMap) {
		modelMap.put("user", acountServices.find(id));
		return  "user/edit";
	}
	
	@RequestMapping(value = "edit", method = RequestMethod.POST)
	public String edit(@ModelAttribute("user") Account account) {		
		acountServices.save(account);
		return  "redirect:/user/index";
	}
}
