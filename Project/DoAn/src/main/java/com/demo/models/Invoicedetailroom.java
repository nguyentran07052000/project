package com.demo.models;
// Generated Dec 16, 2020, 8:55:54 AM by Hibernate Tools 5.2.12.Final

import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Invoicedetailroom generated by hbm2java
 */
@Entity
@Table(name = "invoicedetailroom", catalog = "homiehotels")
public class Invoicedetailroom implements java.io.Serializable {

	private InvoicedetailroomId id;
	private Invoices invoices;
	private Rooms rooms;
	private Double totalRoom;
	private Date dateFrom;
	private Date dateTo;

	public Invoicedetailroom() {
	}

	public Invoicedetailroom(InvoicedetailroomId id, Invoices invoices, Rooms rooms, Date dateFrom, Date dateTo) {
		this.id = id;
		this.invoices = invoices;
		this.rooms = rooms;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}

	public Invoicedetailroom(InvoicedetailroomId id, Invoices invoices, Rooms rooms, Double totalRoom, Date dateFrom,
			Date dateTo) {
		this.id = id;
		this.invoices = invoices;
		this.rooms = rooms;
		this.totalRoom = totalRoom;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}

	@EmbeddedId

	@AttributeOverrides({
			@AttributeOverride(name = "invoiceId", column = @Column(name = "invoiceId", nullable = false)),
			@AttributeOverride(name = "roomId", column = @Column(name = "roomId", nullable = false)) })
	public InvoicedetailroomId getId() {
		return this.id;
	}

	public void setId(InvoicedetailroomId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoiceId", nullable = false, insertable = false, updatable = false)
	public Invoices getInvoices() {
		return this.invoices;
	}

	public void setInvoices(Invoices invoices) {
		this.invoices = invoices;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "roomId", nullable = false, insertable = false, updatable = false)
	public Rooms getRooms() {
		return this.rooms;
	}

	public void setRooms(Rooms rooms) {
		this.rooms = rooms;
	}

	@Column(name = "totalRoom", precision = 22, scale = 0)
	public Double getTotalRoom() {
		return this.totalRoom;
	}

	public void setTotalRoom(Double totalRoom) {
		this.totalRoom = totalRoom;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "dateFrom", nullable = false, length = 10)
	public Date getDateFrom() {
		return this.dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "dateTo", nullable = false, length = 10)
	public Date getDateTo() {
		return this.dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

}
