package com.demo.rervices;

import com.demo.models.Invoidetailservice;
import com.demo.models.InvoidetailserviceId;

public interface InvoiceDetailsServiceServices {

	public Iterable<Invoidetailservice> findAll();
	public Invoidetailservice find(InvoidetailserviceId id);
	public Invoidetailservice save(Invoidetailservice invoidetailservice);
	public void delete(InvoidetailserviceId id);
}
