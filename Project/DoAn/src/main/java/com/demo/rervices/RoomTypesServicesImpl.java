package com.demo.rervices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.demo.models.Roomtypes;
import com.demo.repositories.RoomTypesRepository;

@Service("roomTypesServices")
public class RoomTypesServicesImpl implements RoomTypesServices{

	
	@Autowired
	private RoomTypesRepository roomTypesRepository;

	@Override
	public Iterable<Roomtypes> findAll() {
		return roomTypesRepository.findAll();
	}

	@Override
	public Roomtypes find(int id) {
		return roomTypesRepository.findById(id).get();
	}

	@Override
	public Roomtypes save(Roomtypes roomtypes) {
		return roomTypesRepository.save(roomtypes);
	}

	@Override
	public void delete(int id) {
		roomTypesRepository.deleteById(id);
		
	}
}
