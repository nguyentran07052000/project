package com.demo.rervices;


import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;


import com.demo.models.Account;
import com.demo.models.Role;

public interface AccountServices extends UserDetailsService{

	public Iterable<Account> findAll();
	public Account find(int id);
	public Account save(Account account);
	public void delete(int id);
	List<Account> listAccount();
	List<Role> listRole();
}
