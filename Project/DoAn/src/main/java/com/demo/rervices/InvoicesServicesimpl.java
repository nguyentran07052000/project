package com.demo.rervices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.models.Invoices;
import com.demo.repositories.InvoicesResponsitory;

@Service("invoicesServices")
public class InvoicesServicesimpl implements InvoicesServices{

	@Autowired
	private InvoicesResponsitory invoicesResponsitory;

	@Override
	public Iterable<Invoices> findAll() {
		return invoicesResponsitory.findAll();
	}

	@Override
	public Invoices find(int id) {
		return invoicesResponsitory.findById(id).get();
	}

	@Override
	public Invoices save(Invoices invoices) {
		return invoicesResponsitory.save(invoices);
	}

	@Override
	public void delete(int id) {
		invoicesResponsitory.deleteById(id);
		
	}
}
