package com.demo.rervices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.models.Invoidetailservice;
import com.demo.models.InvoidetailserviceId;

import com.demo.repositories.InvoicesDetailsServiceResponsitory;

@Service("invoiceDetailsService")
public class InvoiceDetailsServiceServicesImpl implements InvoiceDetailsServiceServices{

	@Autowired
	private InvoicesDetailsServiceResponsitory invoicesDetailsServiceResponsitory;

	@Override
	public Iterable<Invoidetailservice> findAll() {
		return invoicesDetailsServiceResponsitory.findAll();
	}

	@Override
	public Invoidetailservice find(InvoidetailserviceId id) {
		return invoicesDetailsServiceResponsitory.findById(id).get();
	}

	@Override
	public Invoidetailservice save(Invoidetailservice invoidetailservice) {
		return invoicesDetailsServiceResponsitory.save(invoidetailservice);
	}

	@Override
	public void delete(InvoidetailserviceId id) {

		invoicesDetailsServiceResponsitory.deleteById(id);
		
	}



	
}
