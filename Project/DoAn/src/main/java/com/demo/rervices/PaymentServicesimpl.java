package com.demo.rervices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.models.Payment;
import com.demo.repositories.PaymentResponsitory;

@Service("paymentServices")
public class PaymentServicesimpl implements PaymentServices{

	@Autowired
	private PaymentResponsitory paymentResponsitory;

	@Override
	public Iterable<Payment> findAll() {
		return paymentResponsitory.findAll();
	}

	@Override
	public Payment find(int id) {
		return paymentResponsitory.findById(id).get();
	}

	@Override
	public Payment save(Payment payment) {
		return paymentResponsitory.save(payment);
	}

	@Override
	public void delete(int id) {
		paymentResponsitory.deleteById(id);
		
	}
}
