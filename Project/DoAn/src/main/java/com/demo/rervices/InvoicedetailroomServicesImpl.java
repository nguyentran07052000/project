package com.demo.rervices;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.models.Invoicedetailroom;
import com.demo.models.InvoicedetailroomId;

import com.demo.repositories.InvoiceDetailRoomRepository;


@Service("invoicedetailroom")
public class InvoicedetailroomServicesImpl implements InvoicedetailroomServices{

	@Autowired
	private InvoiceDetailRoomRepository invoiceDetailRoomRepository;

	@Override
	public Iterable<Invoicedetailroom> findAll() {
		return invoiceDetailRoomRepository.findAll();
	}

	@Override
	public Invoicedetailroom find(InvoicedetailroomId id) {
		return invoiceDetailRoomRepository.findById(id).get();
	}

	@Override
	public Invoicedetailroom save(Invoicedetailroom invoicedetailroom) {
		return invoiceDetailRoomRepository.save(invoicedetailroom);
	}

	@Override
	public void delete(InvoicedetailroomId id) {
		invoiceDetailRoomRepository.deleteById(id);
		
	}

}
