package com.demo.rervices;

import com.demo.models.Invoices;

public interface InvoicesServices {

	public Iterable<Invoices> findAll();
	public Invoices find(int id);
	public Invoices save(Invoices invoices);
	public void delete(int id);
}
