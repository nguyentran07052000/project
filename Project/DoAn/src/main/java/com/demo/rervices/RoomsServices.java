package com.demo.rervices;


import com.demo.models.Rooms;

public interface RoomsServices {
	public Iterable<Rooms> findAll();
	public Rooms find(int id);
	public Rooms save(Rooms rooms);
	public void delete(int id);
}
