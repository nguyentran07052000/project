package com.demo.rervices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.models.Services;
import com.demo.repositories.ServicesRepository;

@Service("servicesServices")
public class ServicesServicesImpl implements ServicesServices{

	
	@Autowired
	private ServicesRepository servicesRepository;

	@Override
	public Iterable<Services> findAll() {
		return servicesRepository.findAll();
	}

	@Override
	public Services find(int id) {
		return servicesRepository.findById(id).get();
	}

	@Override
	public Services save(Services services) {
		return servicesRepository.save(services);
	}

	@Override
	public void delete(int id) {
		servicesRepository.deleteById(id);
		
	}
}
