package com.demo.rervices;

import com.demo.models.Roomtypes;

public interface RoomTypesServices {
	public Iterable<Roomtypes> findAll();
	public Roomtypes find(int id);
	public Roomtypes save(Roomtypes roomtypes);
	public void delete(int id);
}
