package com.demo.rervices;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.demo.models.Account;
import com.demo.models.Role;
import com.demo.repositories.AccountRepository;
import com.demo.repositories.RoleRepository;


@Service("accountServices")
public class AccountServicesimpl implements AccountServices {

	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private RoleRepository roleRepository;


	@Override
	public Iterable<Account> findAll() {
		return accountRepository.findAll();
	}

	@Override
	public Account find(int id) {

		return accountRepository.findById(id).get();
	}

	@Override
	public Account save(Account account) {
		return accountRepository.save(account);
	}

	@Override
	public void delete(int id) {
		accountRepository.deleteById(id);
	}


	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account = accountRepository.findByUsername(username);
		if(account == null) {
			throw new UsernameNotFoundException("Username not found for " + username);
		}
		//chua quyen tai khoan
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for(Role role : account.getRoles()) {
			authorities.add(new SimpleGrantedAuthority(role.getName()));
		}
		return new User(account.getUsername(), account.getPassword(), authorities);
			
			
	}





	@Override
	public List<Account> listAccount() {
		
		return accountRepository.accountList();
	}

	@Override
	public List<Role> listRole() {
		
		return roleRepository.listRole();
	}


	
}
