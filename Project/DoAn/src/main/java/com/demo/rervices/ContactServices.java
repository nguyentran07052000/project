package com.demo.rervices;

public interface ContactServices {

	public void send(String fromAddress, String toAddress, String subject, String content) throws Exception;
}
