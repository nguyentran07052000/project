package com.demo.user;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.demo.models.Account;
import com.demo.rervices.AccountServices;
import com.demo.rervices.RoleServices;

@Controller
@RequestMapping({ "account"})
public class AccountController extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private AccountServices accountServices;
	@Autowired
	private RoleServices roleServices;
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login(@RequestParam(value = "error", required = false) String error, 
						@RequestParam(value = "lougout", required = false) String logout,
			ModelMap modelMap) {
		if(error != null) {
			modelMap.put("msg", "Your username or password was incorrect !");
		}
		if(logout != null) {
			modelMap.put("msg", "Logout Successful");
		}
		
		return "account/login";
	}
	
	
	@RequestMapping(value = "welcome", method = RequestMethod.GET)
	public String welcome(Authentication authentication, ModelMap modelMap) {
		modelMap.put("username", authentication.getName());
		return "account/welcome";
	}
	
	@RequestMapping(value = "register", method = RequestMethod.GET)
	public String register(ModelMap modelMap) {
		modelMap.put("account", new Account());
		modelMap.put("roles", roleServices.findAll());
		return "account/register";
	}
	
	@RequestMapping(value = "register", method = RequestMethod.POST)
	public String register(@ModelAttribute("account") Account account, HttpServletRequest httpServletRequest) {
		String hash = new BCryptPasswordEncoder().encode(account.getPassword());
		account.setPassword(hash);
		String[] roles = httpServletRequest.getParameterValues("roles");
		for (String role : roles) {
			account.getRoles().add(roleServices.find(Integer.parseInt(role)));
			accountServices.save(account);
		}
		return "account/login";
	}
	
	@RequestMapping(value = "accessDenied", method = RequestMethod.GET)
	public String accessdenied() {
		return "account/accessDenied";
	}
}
