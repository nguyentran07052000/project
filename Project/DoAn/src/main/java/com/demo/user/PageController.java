package com.demo.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demo.models.Contact;
import com.demo.rervices.ContactServices;
import com.demo.rervices.RoomsServices;

@Controller
@RequestMapping({"product"})
public class PageController {
	
	@Autowired
	private ContactServices contactServices;
	
	@RequestMapping(value = "about", method = RequestMethod.GET)
	public String flowers() {
		return "product.about";
	}
	
	@RequestMapping(value = "service", method = RequestMethod.GET)
	public String service() {
		return "product.service";
	}
	
	@RequestMapping(value = "blog", method = RequestMethod.GET)
	public String blog() {
		return "product.blog";
	}
	
	@RequestMapping(value = "blog-single", method = RequestMethod.GET)
	public String blog_single() {
		return "product.blog-single";
	}
	
	@RequestMapping(value = "room", method = RequestMethod.GET)
	public String room() {
		return "product.room";
	}
	
	@RequestMapping(value = "room-single", method = RequestMethod.GET)
	public String room_single() {
		return "product.room-single";
	}
	
	@RequestMapping(value = "contact", method = RequestMethod.GET)
	public String contact() {
		return "product.contact";
	}
	
	@RequestMapping(value = "booking", method = RequestMethod.GET)
	public String booking() {
		return "product.booking";
	}
	
	@RequestMapping( method = RequestMethod.GET)
	public String contact(ModelMap modelMap) {
		modelMap.put("contact", new Contact());
		return "product.contact";
	}
	
	@RequestMapping(value = "contact", method = RequestMethod.POST)
	public String send(@ModelAttribute("contact")Contact contact, ModelMap modelMap) {
		try {
			String content = "Name " + contact.getName();
			
			contactServices.send(contact.getEmail(), "phamnguyen100896@gmail.com", contact.getSubject(), content);
			modelMap.put("msg", "Done!");
		} catch (Exception e) {
			modelMap.put("Error", e.getMessage());
		}
		
		return "product.contact";
	}
	
	
}
