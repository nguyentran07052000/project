package com.demo.repositories;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

import com.demo.models.Invoidetailservice;
import com.demo.models.InvoidetailserviceId;


@Repository("InvoicesDetailsServiceResponsitory")
public interface InvoicesDetailsServiceResponsitory extends CrudRepository<Invoidetailservice, InvoidetailserviceId>{

}
