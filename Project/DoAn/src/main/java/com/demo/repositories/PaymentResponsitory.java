package com.demo.repositories;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

import com.demo.models.Payment;

@Repository("PaymentResponsitory")
public interface PaymentResponsitory extends CrudRepository<Payment, Integer>{

}
