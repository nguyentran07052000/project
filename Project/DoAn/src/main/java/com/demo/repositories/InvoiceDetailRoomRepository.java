package com.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.models.Invoicedetailroom;
import com.demo.models.InvoicedetailroomId;


@Repository("InvoiceDetailRoomRepository")
public interface InvoiceDetailRoomRepository extends CrudRepository<Invoicedetailroom, InvoicedetailroomId>{

}
