package com.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.models.Invoices;

@Repository("InvoicesResponsitory")
public interface InvoicesResponsitory extends CrudRepository<Invoices, Integer>{

}
