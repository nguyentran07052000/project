package com.demo.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.demo.models.Account;

@Repository("AccountRepository")
public interface AccountRepository extends JpaRepository<Account, Integer>{

	@Query("from Account where username = :username")
	public Account findByUsername(@Param("username") String username);
	
	@Query("from Account")
	public List<Account> accountList();
}
