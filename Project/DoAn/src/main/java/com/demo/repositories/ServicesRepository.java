package com.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import com.demo.models.Services;

@Repository("ServicesRepository")
public interface ServicesRepository extends CrudRepository<Services, Integer>{

}
