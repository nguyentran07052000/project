package com.demo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.cors().and().csrf().disable();
		
		http.authorizeRequests()
		.antMatchers("/admin/**").access("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_ADMIN')")
		//LOGIN
		.and()
		.formLogin().loginPage("/account/login")
		.loginProcessingUrl("/account/process-login")
		//login thanh cong
		.defaultSuccessUrl("/home")
		//login that bai
		.failureUrl("/account/login?error")
		.usernameParameter("username")
		.passwordParameter("password")
		//LOGOUT
		.and()
		.logout().logoutUrl("/account/logout")
		.logoutSuccessUrl("/account/login?lougout")
		//Quyền
		.and()
		.exceptionHandling().accessDeniedPage("/account/accessDenied")
		;
	}

	@Bean
	public BCryptPasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}
	
}
