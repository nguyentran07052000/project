<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">



	<!-- Reference Google fonts -->
	<link href="${pageContext.request.contextPath }/resources/user/css/BookCss.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath }/resources/user/css/BooksCss.css" rel="stylesheet">

	<!-- Reference Bootstrap and local files -->
	<link rel="stylesheet" href="${pageContext.request.contextPath }/resources/user/css/BookCss.css/Book-font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/resources/user/css/BookCss.css/Book-bootstrap.min.css">

	<link rel="stylesheet" th:href="${pageContext.request.contextPath }/resources/user/css/Book-slick.css">
	<link rel="stylesheet" th:href="${pageContext.request.contextPath }/resources/user/css/Book-slick-theme.css">
	<link rel="stylesheet" th:href="${pageContext.request.contextPath }/resources/user/css/Book-reservation.css">



<body>
	<!-- Hotel Booking Section -->
	<div class="container">
		<h2 class="sub-title" id="book">Hotel Booking</h2>
		<div class="hotel_reserve_box card">
			<h3>Booking Form</h3>
			<br>

			<!-- Reservation form -->
			<form id="resForm" name="resForm" th:action="@{/proceed-reservation}" th:object="${newRes}" method="POST">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				
				
				<!-- Period of Stay -->
				<div class="form-group">
					<label>Period of Stay</label> <input class="form-control prc" id="stay-per" th:field="*{stayPeriod}"
						placeholder="Days" pattern="^(?:[1-9]|[1-4][0-9]|50)$" type="number" required />
				</div>

				<!-- Number of Rooms -->
				<div class="form-group">
					<label>Number of room/suite</label> <input class="form-control prc" id="room_number"
						th:field="*{rooms}" pattern="^(?:[1-9]|[1-4][0-9]|50)$" type="number" placeholder="Rooms"
						required />
				</div>

				<!-- Number of Persons -->
				<div class="form-group">
					<label>Number of persons</label> <input class="form-control prc" id="person_number"
						placeholder="Persons (+18)" type="number" pattern="^(?:[1-9]|[1-4][0-9]|50)$"
						th:field="*{persons}" required />
				</div>

				<!-- Number of Children -->
				<div class="form-group">
					<label>Number of children</label> <input class="form-control prc" id="child_number"
						placeholder="Children (+6)" type="number" pattern="^(?:[0-9]|[1-4][0-9]|50)$"
						th:field="*{children}" required />
				</div>

				<!-- Restaurant Buffet -->
				<div class="form-group">
					<label>Restaurant Open Buffet</label> <select class="form-control prc" id="res_buffet"
						th:field="*{openBuffet}" required>
						<option value="YES">Yes</option>
						<option value="NO">No</option>
					</select>
				</div>

				<!-- Arrival Date -->
				<div class="form-group">
					<label>Visiting start from date </label> <br> <label class="date-label">Arrival Date:</label><input
						th:field="*{arrivalDate}" type="date" id="dateTime_arr" name="dateTime" required />
				</div>

				<!-- hidden field to pass next live price value in this label to complete reservation -->
				<div class="form-group">
					<input type="hidden" id="priceField" value="" th:field="*{price}" />
				</div>

				<!-- Live price count -->
				<div class="form-group">
					<div class="price-lab">
						TOTAL: <label><output class="price-fi" id="price-count"></output></label>$
					</div>
				</div>

				<!-- submit form -->
				<button class="sub-btn">Submit</button>
			</form>
		</div>
	</div>


	<!-- Reference Javascript and local files -->
	<script src="${pageContext.request.contextPath }/resources/user/js/Book-jquery.min.js"></script>
	<script src="${pageContext.request.contextPath }/resources/user/js/Book-slick.css/Book-bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath }/resources/user/js/Book-slick.css/Book-js"></script>

	<script th:src="${pageContext.request.contextPath }/resources/user/scripts/Book-reservation.js"></script>
	<script th:src="${pageContext.request.contextPath }/resources/user/scripts/Book-cost.js"></script>

</body>

</html>